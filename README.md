# iCloud for Linux

An electron app for iCloud made with [Nativefier](https://github.com/jiahaog/Nativefier).

## To Install

1. Clone this project using `git clone https://gitlab.com/tomthecat/icloud-for-linux`.
2. Change into its directory using `cd icloud-for-linux`.
3. Execute the installer by running `./install.sh`.

## To Uninstall

1. Change into directory using `cd //opt/icloud-for-linux`.
2. Execute the uninstaller by running `./uninstall.sh`.